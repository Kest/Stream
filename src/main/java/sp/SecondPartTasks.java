package sp;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public final class SecondPartTasks {

    private SecondPartTasks() {}

    // Найти строки из переданных файлов, в которых встречается указанная подстрока.
    public static List<String> findQuotes(List<String> paths, CharSequence sequence) {
        return paths.stream().filter(x -> x.contains(sequence)).collect(Collectors.toList());
    }

    // В квадрат с длиной стороны 1 вписана мишень.
    // Стрелок атакует мишень и каждый раз попадает в произвольную точку квадрата.
    // Надо промоделировать этот процесс с помощью класса java.util.Random и посчитать, какова вероятность попасть в мишень.
    public static double piDividedBy4() {
        return Stream.iterate(0, x -> x + 1)
                .limit(1000)
                .map(x -> new Random().nextDouble())
                .filter(x -> x < 0.5*0.5*Math.PI)
                .collect(Collectors.toList())
                .size()/1000.0;
    }

    // Дано отображение из имени автора в список с содержанием его произведений.
    // Надо вычислить, чья общая длина произведений наибольшая.
    public static String findPrinter(Map<String, List<String>> compositions) {
        return compositions.entrySet().stream().collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()
                .toString())).entrySet().stream().sorted(Map.Entry.comparingByValue()).map(Map.Entry::getKey)
                .collect(Collectors.toList()).stream().findFirst().get();
    }

    // Вы крупный поставщик продуктов. Каждая торговая сеть делает вам заказ в виде Map<Товар, Количество>.
    // Необходимо вычислить, какой товар и в каком количестве надо поставить.
    public static Map<String, Integer> calculateGlobalOrder(List<Map<String, Integer>> orders) {
//        return orders.stream().collect(Collectors.groupingBy())
//        return orders.stream().map(Collectors.toMap())
        return null;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            System.out.println(piDividedBy4());
        }
    }
}